import React from 'react';
import './assets/App.css';
import SearchHost from "./features/SearchHost";

function App() {
    return (
        <SearchHost />
    );
}

export default App;
