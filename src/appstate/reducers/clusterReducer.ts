import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import ShortyCluster, {LightWeightCluster} from "../../features/shorty/ShortyCluster";
import {setCrashed, setErrorMessage, setServerStatus} from "./serverStatusReducer";
import {ClusterResult, get_clustering_results, get_summary, SummarizationResult} from "../../models/api/cluster";

interface ClusterState {
    clusters: ShortyCluster[]
    cluster_data: any
    cluster_loading: boolean
    summary_loading: boolean
    visible_cluster: string | null
    clusters_loaded: boolean
}

const initialState: ClusterState = {
    clusters: [],
    cluster_data: {"extra_data":{}},
    cluster_loading: false,
    summary_loading: false,
    visible_cluster: null,
    clusters_loaded: false
};

export const fetchClustering = createAsyncThunk('cluster/fetchClustering', async (selected_weighted_oas: number[][], thunkAPI) => {
    let selected_oas = selected_weighted_oas.map((x) => x[0]);
    let oa_weights = selected_weighted_oas.map(x => x[1]);
    thunkAPI.dispatch(setServerStatus("Clustering..."))
    const response = await get_clustering_results(
        {
            db: "systematic",
            cluster_settings: {
                selected_oas,
                oa_weights,
                // amount_documents: [],
                scoring_method: "penalized_average_score",
                clustering_method: "textual_clustering",
                db_name: "systematic"
            }
        }
    ).then(x => {
        thunkAPI.dispatch(setServerStatus("Waiting..."));
        return x;
    })
        .catch(err => {
            thunkAPI.dispatch(setCrashed())
            thunkAPI.dispatch(setServerStatus(err.toString()))
        })
    if (response == null) {
        thunkAPI.dispatch(setErrorMessage("Failed to fetch clustering results."))
        return thunkAPI.rejectWithValue("Something went wrong fetching the result.")
    }
    return response as ClusterResult
})

export const fetchSummary = createAsyncThunk('cluster/fetchClusterSummary', async (visible_cluster: LightWeightCluster, thunkAPI) => {
    thunkAPI.dispatch(setServerStatus("Summarizing..."))
    const response = await get_summary(
        {
            cluster: visible_cluster,
            summarization_settings: {
                db_name: "systematic"
            }
        }
    ).then(x => {
        thunkAPI.dispatch(setServerStatus("Waiting..."));
        return x;
    })
        .catch(err => {
            thunkAPI.dispatch(setCrashed())
            thunkAPI.dispatch(setServerStatus(err.toString()))
        })
    if (response == null) {
        thunkAPI.dispatch(setErrorMessage("Failed to fetch summary results."))
        return thunkAPI.rejectWithValue("Something went wrong fetching the result.")
    }
    return response as SummarizationResult
})

export const ClusterSlice = createSlice({
    name: 'cluster',
    initialState,
    reducers: {
        resetClusters: state => {
            state.clusters = [];
            state.clusters_loaded = false;
        },
        setVisibleCluster: (state, action: PayloadAction<string>) => {
            state.visible_cluster = action.payload
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchClustering.pending, (state, action) => {
            state.cluster_loading = true
        });
        builder.addCase(fetchClustering.fulfilled, (state, action) => {
            state.cluster_loading = false
            let clustering_method = action.payload.clustering_method;
            if(clustering_method === "organism_based"){
                state.clusters = action.payload.data.map((x: any) => ShortyCluster.from_json(x))
            }else{
                if(clustering_method==="textual_clustering"){
                    // New clustering method, should just store the cluster data as we pass it to headstart for processing
                    state.cluster_data = action.payload.data;
                    state.clusters_loaded = true;
                    // The boolean here kicks off the headstart visualisation!
                }
            }
        });
        builder.addCase(fetchClustering.rejected, (state, action) => {
            state.cluster_loading = false
        });

        builder.addCase(fetchSummary.pending, (state, action) => {
            state.summary_loading = true
        });
        builder.addCase(fetchSummary.fulfilled, (state, action) => {
            // console.log("apl", action.payload.result);
            let cluster: ShortyCluster = action.payload.result;
            let cluster_index = state.clusters.findIndex(y => cluster.id_ === y.id_);
            let new_clusters = state.clusters.slice()
            new_clusters[cluster_index] = cluster
            state.clusters = new_clusters
            state.summary_loading = false
            // state.clusters = action.payload.clusters.map((x:any)=>ShortyCluster.from_json(x))
        });
        builder.addCase(fetchSummary.rejected, (state, action) => {
            state.summary_loading = false
        });
    }
});

export const {resetClusters, setVisibleCluster} = ClusterSlice.actions;


export default ClusterSlice.reducer;