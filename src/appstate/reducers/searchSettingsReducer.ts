import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from '../store';
import {get_search_results, QueryResult, QueryResultInfo} from "../../models/api/search";
import {setCrashed, setServerStatus, setErrorMessage} from './serverStatusReducer';
import {fetchClustering, resetClusters} from "./clusterReducer";

interface SearchSettingsState {
    settings_visible: boolean;

    search_loading: boolean;
    result: Array<number>[];
    kneepoint: number

    textBoxData: string;
    tokenized_query: string[]
    query_has_changed: boolean

    search_result: Array<number>[] | null
    query_info: QueryResultInfo | { lemma_suggestions: [] }
}

const initialState: SearchSettingsState = {
    settings_visible: false,
    textBoxData: "",
    search_loading: false,
    tokenized_query: [],
    query_has_changed: false,

    result: [],
    kneepoint: 0,
    search_result: null,
    query_info: {lemma_suggestions: []}

};

export const fetchSearchResult = createAsyncThunk('posts/fetchPosts', async (args, thunkAPI) => {
    let state = thunkAPI.getState() as RootState;
    thunkAPI.dispatch(resetClusters())
    thunkAPI.dispatch(setServerStatus("Searching..."))
    const response = await get_search_results(
        {
            query: {
                to_be_tokenized: state.search_settings.textBoxData,
                tokenized_query: state.search_settings.tokenized_query,
            },
            search_settings: {
                db: "systematic",
                filter_lemmatized_query: false
            }
        }
    ).then((x) => {
        if (x.is_error) {
            thunkAPI.dispatch(setCrashed())
            thunkAPI.dispatch(setServerStatus(JSON.stringify(x)))
        }
        thunkAPI.dispatch(fetchClustering(x.result.slice(0, x.kneepoint)))
        return x;
    })
        .catch(err => {
            thunkAPI.dispatch(setCrashed())
            thunkAPI.dispatch(setServerStatus(err.toString()))

        })
    if (response == null){
        thunkAPI.dispatch(setErrorMessage("Failed to fetch search results."))
        return thunkAPI.rejectWithValue("Something went wrong fetching the result.")
    }else {
        return response as QueryResult
    }
})

export const SearchSettingsSlice = createSlice({
    name: 'search_settings',
    initialState,
    reducers: {
        toggleSearchSettingsVisibility: state => {
            console.log("reducing the search settings");
            state.settings_visible = !state.settings_visible;
        },

        setSearchBoxData: (state, action: PayloadAction<string>) => {
            state.textBoxData = action.payload;
            state.query_has_changed = true;
        },
        addQueryLemma: (state, action: PayloadAction<string>) => {
            state.tokenized_query.push(action.payload);
            state.query_has_changed = true;
        },
        removeQueryLemma: (state, action: PayloadAction<string>) => {
            state.tokenized_query = state.tokenized_query.filter((lemma: string) => lemma !== action.payload);
            state.query_has_changed = true;
        },
        setSearchLoadingTrue: state => {
            state.search_loading = true;
        },
        setSearchLoadingFalse: state => {
            state.search_loading = true;
        }

    },
    extraReducers: builder => {
        builder.addCase(fetchSearchResult.pending, (state, action) => {
            state.search_loading = true
        });
        builder.addCase(fetchSearchResult.fulfilled, (state, action) => {
            state.search_loading = false
            state.result = action.payload.result
            state.kneepoint = action.payload.kneepoint
            state.query_info = action.payload.query_info
            state.query_has_changed = false;
            state.tokenized_query = action.payload.query_info.cleaned_query
            state.textBoxData = ""
        });
        builder.addCase(fetchSearchResult.rejected, (state, action) => {
            state.search_loading = false
        });
    }

});

export const {toggleSearchSettingsVisibility, setSearchBoxData, setSearchLoadingTrue, setSearchLoadingFalse, addQueryLemma, removeQueryLemma} = SearchSettingsSlice.actions;

export const selectSearchSettingsVisible = (state: RootState) => state.search_settings.settings_visible;

export default SearchSettingsSlice.reducer;