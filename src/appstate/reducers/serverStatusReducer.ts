import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {AppThunk, RootState} from '../store';

interface ServerStatusState {
    status: string;
    has_crashed: boolean;
    errorText: string;
}

const initialState: ServerStatusState = {
    status: "Not connected",
    has_crashed: false,
    errorText:""
};

export const ServerStatusSlice = createSlice({
    name: 'server_status',
    initialState,
    reducers: {
        setCrashed: state => {
            state.has_crashed = true;
        },
        // Use the PayloadAction type to declare the contents of `action.payload`
        setServerStatus: (state, action: PayloadAction<string>) => {
            state.status = action.payload;
        },
        setErrorMessage: (state, action: PayloadAction<string>) => {
            state.errorText = action.payload
        }
    },
});

export const {setCrashed, setServerStatus, setErrorMessage} = ServerStatusSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
// export const incrementAsync = (amount: number): AppThunk => dispatch => {
//     setTimeout(() => {
//         dispatch(incrementByAmount(amount));
//     }, 1000);
// };

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectStatus = (state: RootState) => state.server_status.status;
export const selectCrashed = (state: RootState) => state.server_status.has_crashed;

export default ServerStatusSlice.reducer;