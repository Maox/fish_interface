import {createSlice} from '@reduxjs/toolkit';

interface UISettingsReducerState {
    fitness_visibility: boolean;
}

const initialState: UISettingsReducerState = {
    fitness_visibility: false,
};

export const UISettingsReducerSlice = createSlice({
    name: 'server_status',
    initialState,
    reducers: {
        toggleFitnessVisibility: state => {
            state.fitness_visibility = !state.fitness_visibility;
        },
    },
});

export const {toggleFitnessVisibility} = UISettingsReducerSlice.actions;


export default UISettingsReducerSlice.reducer;