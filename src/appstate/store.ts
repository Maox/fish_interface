import {configureStore, ThunkAction, Action, getDefaultMiddleware} from '@reduxjs/toolkit'
import ServerStatusReducer from "./reducers/serverStatusReducer";
import SearchSettingsReducer from "./reducers/searchSettingsReducer";
import ClusterReducer from "./reducers/clusterReducer";
import UISettingsReducer from "./reducers/UISettingsReducer";

export const store = configureStore({
  middleware: getDefaultMiddleware({
      serializableCheck: false,
    }),
  reducer: {
    server_status: ServerStatusReducer,
    search_settings: SearchSettingsReducer,
    cluster: ClusterReducer,
    ui_settings:UISettingsReducer
  }
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<any>
>;

