import React from 'react';
import config from "./headstart-config";
import GillstartRunner from "./GillstartRunner";
import {RootState} from "../../appstate/store";
import {connect} from "react-redux";

// @ts-ignore
import structuredClone from '@ungap/structured-clone';

export interface ClustermapProps {
    cluster_data: any
}

class Clustermap extends React.Component<ClustermapProps, {}> {
    componentDidMount() {
        let runner = new GillstartRunner(config);
        runner.set_backend_data(structuredClone(this.props.cluster_data))
        runner.run();
    }

    render() {
        return <div id={"visualization"}></div>;
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        cluster_data: state.cluster.cluster_data,
    };
};

export default connect(mapStateToProps, {})(Clustermap);