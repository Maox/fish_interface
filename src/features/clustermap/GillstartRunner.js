import HeadstartRunner from "./Headstart/vis/js/HeadstartRunner";
import "jquery-ui/dist/jquery-ui.js"
import "./Headstart/vis/stylesheets/main.scss";
import output from "./manual_querytextual_adhesion_nojson";

class GillstartRunner extends HeadstartRunner {

    set_backend_data(data){
        this.backendData = data;
    }

    async run() {
      this.checkBrowserVersion();
      this.renderReact();
      this.addBackButtonListener();
      // this.backendData = output; // change this, I think.
      this.dispatchDataEvent(this.backendData);
      this.initStore(this.backendData);
      this.applyQueryParams();
      this.addWindowResizeListener();
    }
}

export default GillstartRunner;