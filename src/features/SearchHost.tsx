import React from 'react';
import {Container} from "react-bootstrap";
import HeaderContainer from "./header/HeaderContainer";
import SearchboxContainer from "./searchbox/SearchBoxContainer";
// import OrganismMozaic from "./shorty/OranismMozaic";
import {check_and_load_automation} from "./automated_loading";
import {ErrorBoundary} from "react-error-boundary";
import ErrorHandler from "./errors/ErrorHandler";
// import CustomErrorBoundary from "./errors/CustomErrorBoundary";
import Clustermap from "./clustermap/Clustermap";
import {RootState} from "../appstate/store";
import {connect} from "react-redux";
import {LOADING_GIF} from "../assets/locations";
import DocumentLoadingPlot from "./DocumentLoadingPlot";
import ScatterPlot from "./ScatterPlot";

export interface SearchHostProps {
    clusters_loaded: boolean
    cluster_loading: boolean
    document_loading: number[]
    smooth_d2: number[]
}



class SearchHost extends React.Component<SearchHostProps, {}> {
    componentDidMount() {
        // On mount, check if we want to start loading our test cases
        check_and_load_automation()
    }


    render() {
        return <Container>
            <HeaderContainer/>
            <ErrorBoundary FallbackComponent={ErrorHandler}>
                <SearchboxContainer/>
                {this.props.cluster_loading && <img src={LOADING_GIF} onError={(t) => console.log(t)} alt={"Clustering..."}/>}
                {this.props.clusters_loaded && <Clustermap/>}
                {/*{this.props.clusters_loaded && <ScatterPlot data={this.props.document_loading} xtitle={"Paper"} ytitle={"Weights"}/>}*/}
                {/*{this.props.clusters_loaded && <ScatterPlot data={this.props.smooth_d2} xtitle={"Paper"} ytitle={"Weights"}/>}*/}

                {/*<OrganismMozaic/>*/}
            </ErrorBoundary>
            {/*<div className={"fixed-bottom container"}>*/}
            {/*    <small>This website uses cookies to collate search operations and gratefully uses content licensed under Creative Commons. Organism images are from Wikimedia Commons.</small>*/}
            {/*</div>*/}

        </Container>;
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        clusters_loaded: state.cluster.clusters_loaded,
        cluster_loading: state.cluster.cluster_loading,
        document_loading: state.cluster.cluster_data.extra_data.document_weights,
        smooth_d2: state.cluster.cluster_data.extra_data.smooth_d2
    };
};

export default connect(mapStateToProps, {})(SearchHost);