import {store} from "../appstate/store";
import {fetchSearchResult, setSearchBoxData} from "../appstate/reducers/searchSettingsReducer";
import {fetchSummary, setVisibleCluster} from "../appstate/reducers/clusterReducer";


export function check_and_load_automation(){
    let autokey = findGetParameter("auto");
    if(map_test_names_to_function.hasOwnProperty(autokey)) {
        console.log("Starting automation")
        map_test_names_to_function[autokey]()
    }
}

function findGetParameter(parameterName:string) {
    var result = "",
        tmp = [];
    window.location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function adhesion_to_cluster(){
    store.dispatch(setSearchBoxData("temporary adhesion"));
    // store.dispatch(fetchSearchResult());
    store.dispatch(setVisibleCluster("f76619b51b8206d2b62bccbe3f58b841_11"))
    let simple_cluster = {"docids":[131253,65760,9325,132513,9985,1989,76088,60438,3782,45768,86917,5637,22048,22686,104718,112911,112927,137705,48002,121904,146637,23802],"id_":"f76619b51b8206d2b62bccbe3f58b841_11","name":"Formicidae","orgid":36668,"score":0.03202403335018818, "organism_image":"http"};
    store.dispatch(fetchSummary(simple_cluster))
}

let map_test_names_to_function: any = {
    "adhesion_to_cluster": adhesion_to_cluster
}