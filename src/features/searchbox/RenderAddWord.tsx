import * as React from "react";
import {connect} from "react-redux";
import AddCircleIcon from '@material-ui/icons/AddCircle';
import {Badge} from "react-bootstrap";
import RemoveCircle from "@material-ui/icons/RemoveCircle";
import {AppDispatch, RootState} from "../../appstate/store";
import { removeQueryLemma, addQueryLemma } from "../../appstate/reducers/searchSettingsReducer";

export interface RenderAddWordProps {
    //These are the props we should pass to the current component.
    word: string;
    add_query_lemma: (lemma: string) => void;
    remove_query_lemma: (lemma: string) => void;
    inverse: boolean;
}

class RenderAddWord extends React.Component<RenderAddWordProps, {}> {

    static defaultProps = {
        inverse: false
    };

    render() {
        let add_box = <AddCircleIcon
            style={{"width": "1rem", "height": "1rem"}}
            onClick={() => this.props.add_query_lemma(this.props.word)}/>
        let selected_variant: "secondary"|"primary" = "secondary";
        if (this.props.inverse) {
            selected_variant = "primary";
            add_box = <RemoveCircle
                style={{"width": "1rem", "height": "1rem"}}
                onClick={() => this.props.remove_query_lemma(this.props.word)}/>
        }

        return <Badge pill variant={selected_variant} key={this.props.word}>{add_box}{this.props.word}</Badge>
    }
}


function mapDispatchToProps(dispatch:AppDispatch) {
    return {
        remove_query_lemma: (query: string) => dispatch(removeQueryLemma(query)),
        add_query_lemma: (query: string) => dispatch(addQueryLemma(query)),
    };
}

const mapStateToProps = (state: RootState) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RenderAddWord);