/**
 * Created by Mart Willocx on 24/06/2019 - 18:15.
 *
 */

import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import {connect} from "react-redux";
import {AppDispatch, RootState} from "../../appstate/store";
import {fetchSearchResult, setSearchBoxData} from "../../appstate/reducers/searchSettingsReducer";
import {Col, Row} from "react-bootstrap";
import RenderAddWord from "./RenderAddWord";
import ReduxErrorMessage from "../errors/ReduxErrorMessage";

export interface SearchBoxProps {
    setSearchBoxData: (data: string) => void
    searchBoxData: string
    startSearch: () => void
    query_has_changed: boolean
    tokenized_query: string[]
    errorText: string
}

class SearchBox extends React.Component<SearchBoxProps, {}> {
    render() {
        let button_variant_selected: "primary" | "secondary" = this.props.query_has_changed ? "primary" : "secondary"
        let search_button_text = this.props.query_has_changed ? "Update the search!" : "Search is up-to-date"

        let button_placeholder = this.props.tokenized_query.length < 1 ? "The essential function is..." : "Add a function"

        return <div><Row style={{"marginBottom": "1rem"}}>
            {(this.props.tokenized_query.length > 0) && <Col md={"auto"}>
                {this.props.tokenized_query.map(x => <RenderAddWord inverse={true} key={x + "-selected"} word={x}/>)}
            </Col>}
            <Col md={5}>
                <Form>
                    <InputGroup className="">
                        <FormControl
                            placeholder={button_placeholder}
                            aria-label={button_placeholder}
                            aria-describedby={button_placeholder}
                            onChange={(e) => this.props.setSearchBoxData(e.target.value)}
                            onKeyDown={(e: any) => {
                                const keyName = e.key;
                                if (keyName === "Enter") {
                                    e.preventDefault();
                                    this.props.startSearch()
                                }
                            }}
                            value={this.props.searchBoxData}
                            className={"search_box"}
                        />
                        <InputGroup.Append>
                            <Button
                                variant={button_variant_selected}
                                onClick={() => {
                                    this.props.startSearch()
                                }}
                                className={"search_button"}
                            >{search_button_text}</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </Form>
            </Col>
        </Row>
        {
            this.props.errorText.length > 1 &&
            <Row>
                <Col>
                    <ReduxErrorMessage/>
                </Col>
            </Row>
        }
        </div>
    }
}

function mapDispatchToProps(dispatch: AppDispatch) {
    return {
        setSearchBoxData: (query: string) => dispatch(setSearchBoxData(query)),
        startSearch: () => dispatch(fetchSearchResult())
    };
}

const mapStateToProps = (state: RootState) => {
    return {
        searchBoxData: state.search_settings.textBoxData,
        query_has_changed: state.search_settings.query_has_changed,
        tokenized_query: state.search_settings.tokenized_query,
        errorText: state.server_status.errorText
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);
