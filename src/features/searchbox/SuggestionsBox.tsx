import React from "react";
import RenderAddWord from "./RenderAddWord";
import {Row} from "react-bootstrap";
import {AppDispatch, RootState} from "../../appstate/store";
import {connect} from "react-redux";
import {LOADING_GIF} from "../../assets/locations";

export interface SuggestionsBoxProps {
    lemma_suggestions: null | string[]
    tokenized_query: string[]
    is_searching: boolean
}

class SuggestionsBox extends React.Component<SuggestionsBoxProps, {}> {
    render() {
        return <Row>
            {this.props.is_searching && <img src={LOADING_GIF} onError={(t) => console.log(t)} alt={"Loading..."}/>}
            {this.props.lemma_suggestions &&
            this.props.lemma_suggestions.filter((lemma: string) => this.props.tokenized_query.indexOf(lemma) < 0)
                .map((lemma: string) => <RenderAddWord word={lemma} key={lemma + "-suggested"}/>)
            }
        </Row>
    }
}


const mapStateToProps = (state: RootState) => {
    return {
        lemma_suggestions: state.search_settings.query_info.lemma_suggestions,
        tokenized_query: state.search_settings.tokenized_query,
        is_searching: state.search_settings.search_loading
    };
};

function mapDispatchToProps(dispatch: AppDispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(SuggestionsBox);