import React from "react";
import SearchBox from "./SearchBox";
import SuggestionsBox from "./SuggestionsBox";

export interface SearchboxContainerProps {
    //These are the props we should pass to the current component.
}

class SearchboxContainer extends React.Component<SearchboxContainerProps, {}> {
    render() {
        return <div>
            <SearchBox />
            <SuggestionsBox />
        </div>
    }
}

export default SearchboxContainer;