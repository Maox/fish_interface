import * as React from "react";
import {Card} from "react-bootstrap";
import {FallbackProps} from "react-error-boundary";

class ErrorHandler extends React.Component<FallbackProps, {}> {
    render() {
        return <Card>
            <Card.Header>
                <p>
                    Sorry, an error occured whilst processing the biological information. {' '}
                    <span
                        style={{cursor: 'pointer', color: '#0077FF'}}
                        onClick={() => {
                            window.location.reload();
                        }}
                    >
                    Please reload this page.
                  </span>{' '}
                </p>
            </Card.Header>
            <Card.Body>
                <details className="error-details" style={{whiteSpace: "pre-wrap"}}>
                    <summary>Click for error details</summary>
                    {this.props.error.message}
                    {this.props.error.stack && this.props.error.stack.toString()}
                    {/*{this.props.errorText && this.props.errorText}*/}
                    {/*{errorInfo && errorInfo.componentStack.toString()}*/}
                </details>
            </Card.Body>
        </Card>
    }
}

export default ErrorHandler;