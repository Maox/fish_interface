import * as React from "react";
import {connect} from "react-redux";
import {AppDispatch, RootState} from "../../appstate/store";
import {Card} from "react-bootstrap";

export interface ReduxErrorMessageProps {
    //These are the props we should pass to the current component.
    errorText: string
}

class ReduxErrorMessage extends React.Component<ReduxErrorMessageProps, {}> {
    render() {
        return <Card>
              <Card.Header>
                <p>
                  Sorry, there was an error in loading the biological inspiration. {' '}
                  <span
                    style={{ cursor: 'pointer', color: '#0077FF' }}
                    onClick={() => {
                      window.location.reload();
                    }}
                  >
                    Reload this page
                  </span>{' '} or try to update the search with another term.
                </p>
              </Card.Header>
              <Card.Body>
                <details className="error-details">
                  <summary>Click for error details</summary>
                  {this.props.errorText && this.props.errorText}
                    {/*{errorInfo && errorInfo.componentStack.toString()}*/}
                </details>
              </Card.Body>
        </Card>
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        errorText: state.server_status.errorText,
    };
};

function mapDispatchToProps(dispatch: AppDispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(ReduxErrorMessage);