// @ts-ignore
import React from 'react';
// import OrganismMozaic from "./shorty/OranismMozaic";
// import CustomErrorBoundary from "./errors/CustomErrorBoundary";
import {RootState} from "../appstate/store";
import {connect} from "react-redux";

// @ts-ignore

export interface DocumentLoadingPlotProps {
    document_weights: string
    document_weightsd2: string
}

class DocumentLoadingPlot extends React.Component<DocumentLoadingPlotProps, {}> {
    componentDidMount() {
    }


    render() {
        return <div><img src={`data:image/jpeg;base64,${this.props.document_weights}`} />
             <img src={`data:image/jpeg;base64,${this.props.document_weightsd2}`} /></div>
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        document_weights: state.cluster.cluster_data.extra_data.document_weights,
        document_weightsd2: state.cluster.cluster_data.extra_data.document_weights_2d,
    };
};

export default connect(mapStateToProps, {})(DocumentLoadingPlot);