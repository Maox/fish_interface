import * as React from "react";
import {Button, Col, Row} from "react-bootstrap";
import ShortyCluster from "./ShortyCluster";
import ShortyClusterCard from "./ShortyClusterCard";
import {connect} from "react-redux";
import OranismMozaicFigure from "./OrganismMozaicFigure";
import ShortyDocument from "./ShortyDocument";
import Logger from "../../models/logger/LoggerService";
import {AppDispatch, RootState} from "../../appstate/store";
import {fetchSummary, setVisibleCluster} from "../../appstate/reducers/clusterReducer";
import {LOADING_GIF} from "../../assets/locations";

export interface OranismMozaicProps {
    //These are the props we should pass to the current component.
    clusters: ShortyCluster[];
    getClusterSummary: (serialized_cluster: ShortyCluster) => void;
    setVisibleCluster: (visible_cluster: string) => void;
    fitness_visibility: boolean;
    visible_cluster: string;
    cluster_loading:boolean
}

export interface OrganismMozaicState {
    amount_organism_images: number;
}

class OrganismMozaic extends React.Component<OranismMozaicProps, OrganismMozaicState> {
    constructor(props: any) {
        super(props);
        this.state = {
            amount_organism_images: 15
        }
        this.setVisibleCluster = this.setVisibleCluster.bind(this);
        this.showMoreImages = this.showMoreImages.bind(this);
    }

    setVisibleCluster(cluster_id: string) {
        this.props.setVisibleCluster(cluster_id)
        let cluster_index = this.props.clusters.findIndex(y => cluster_id === y.id_);
        let visible_cluster = this.props.clusters[cluster_index];
        this.props.getClusterSummary(visible_cluster);
        Logger.warn("Showing cluster with id: " + JSON.stringify(cluster_id))
    }

    showMoreImages() {
        this.setState({amount_organism_images: this.state.amount_organism_images + 5})
    }

    containsDocID(docid: number, documents: ShortyDocument[]) {
        // console.log("Checking docs", documents);
        for (let i = 0; i < documents.length; i++) {
            const document = documents[i];
            if (document.id === docid) {
                return true;
            }
        }
    }

    render() {
        let cluster_index = this.props.clusters.findIndex(y => this.props.visible_cluster === y.id_);
        let visible_cluster = this.props.clusters[cluster_index];

        let visible_clusters = this.state.amount_organism_images;
        for (let i = visible_clusters; i < this.props.clusters.length; i++) {
            const cluster = this.props.clusters[i];
            if (this.containsDocID(152915, cluster.documents)) {
                visible_clusters = i + 1;
                console.log("Setting visibile clusters to ", visible_clusters);
                break;
            }
        }


        return <div>
            <Row>
                <Col>
                    {this.props.cluster_loading && <img src={LOADING_GIF} onError={(t) => console.log(t)} alt={"Loading..."}/>}
                    {this.props.clusters.slice(0, visible_clusters).map(x =>
                        <OranismMozaicFigure
                            cluster={x}
                            key={"mozi-" + x.id_}
                            setVisibleCluster={this.setVisibleCluster}
                        />)}
                    {this.props.clusters.length > visible_clusters &&
                    <Button onClick={() => this.showMoreImages()}
                            variant={"secondary"}>More...</Button>}
                </Col>
            </Row>
            {visible_cluster && <Row>
                Current visible cluster: {this.props.visible_cluster}
                <ShortyClusterCard
                    cluster={visible_cluster}
                    key={"cluster-card-" + visible_cluster.id_}
                />
            </Row>}</div>
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        fitness_visibility: state.ui_settings.fitness_visibility,
        clusters: state.cluster.clusters,
        visible_cluster: state.cluster.visible_cluster,
        cluster_loading: state.cluster.cluster_loading
    };
};

function mapDispatchToProps(dispatch: AppDispatch) {
    return {
        setVisibleCluster: (visible_cluster: string) => dispatch(setVisibleCluster(visible_cluster)),
        getClusterSummary: (serialized_cluster: ShortyCluster) => dispatch(fetchSummary(serialized_cluster.toLightweight())),
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrganismMozaic);