import * as React from "react";
import ShortyCluster from "./ShortyCluster";
import {Card, Col, Image, ListGroup, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {AppDispatch, RootState} from "../../appstate/store";
import {isProduction} from "../../models/utils";
import ExtractedImageRender from "./ExtractedImageRender";
import {LOADING_GIF} from "../../assets/locations";

export interface ShortyClusterCardProps {
    //These are the props we should pass to the current component.
    cluster: ShortyCluster
    fitness_visibility: boolean
}

class ShortyClusterCard extends React.Component<ShortyClusterCardProps, {}> {
    render() {
        return <Card>
            <Card.Body>
                <Card.Title>
                    {this.props.cluster.name.charAt(0).toUpperCase() + this.props.cluster.name.slice(1)} {this.props.fitness_visibility && this.props.cluster.score &&
                <span>({this.props.cluster.score.toPrecision(2)})</span>}
                </Card.Title>
                <div> {/* this used to be a Card.Text tag.*/}
                    <Row>
                        <Col>
                            {/*{JSON.stringify(this.props.cluster.summary)}*/}
                            {this.props.cluster.summary &&
                            <ListGroup className={"list-group-flush"}>{this.props.cluster.summary.map(y =>
                                <ListGroup.Item
                                key={"scc-"+y}
                                >{y}</ListGroup.Item>)}</ListGroup>}
                            {this.props.cluster.summary && this.props.cluster.summary.length < 1 && <i>Something went wrong in calculating the summary of cluster {this.props.cluster.id_}. Please refresh the page.</i>}
                            {!this.props.cluster.summary && <img src={LOADING_GIF} onError={(t) => console.log(t)} alt={"Loading..."}/>}
                        </Col>


                        {this.props.cluster.images.length > 0 && <Col xs={3}>
                            <Image src={this.props.cluster.images[0].src}
                                   alt={this.props.cluster.images[0].alt}
                                   key={"scc2-"+this.props.cluster.images[0].src}
                                   style={{width: "100%"}}/>
                        </Col>}
                    </Row>
                    <hr/>
                    <Row>
                        {this.props.cluster.images.length > 1 && this.props.cluster.images.slice(1).map(y =>
                            <ExtractedImageRender
                                src={y.src}
                                caption={y.caption}
                                alt={y.alt}
                                key={"eir-" + y.src}
                            />)}
                        {this.props.cluster.images.length < 2 &&
                        <p>Sorry. Not all images are available in the online edition.</p>}
                    </Row>
                    <hr/>
                    {this.props.cluster.documents.map(y =>
                        <Row>
                            <Col>
                                <a href={'https://doi.org/' + y.doi}
                                   target={"_blank"}
                                   key={"scclink-" + y.doi}
                                   rel={"noopener noreferrer"}
                                >{y.doi}</a> - {y.title} {y.weight && this.props.fitness_visibility &&
                            <span>({y.weight.toPrecision(2)})</span>}
                            </Col>
                        </Row>
                    )}
                </div>
            </Card.Body>
        </Card>
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        fitness_visibility: state.ui_settings.fitness_visibility,
    };
};
function mapDispatchToProps(dispatch: AppDispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(ShortyClusterCard);