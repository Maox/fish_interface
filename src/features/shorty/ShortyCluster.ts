/**
 * Created by Mart Willocx on 28/05/2020 - 16:33.
 *
 */
import ShortyDocument, {ShortyDocumentJson} from "./ShortyDocument";
import ShortyImage, {ShortyImageJson} from "./ShortyImage";

export interface ShortyClusterJson {
    name: string
    orgid: number
    score: number
    documents: ShortyDocumentJson[]
    summary: string[]
    images: ShortyImageJson[]
    id_: string,
    organism_image:string
}

export interface LightWeightCluster{
    name: string;
    orgid: number;
    docids: number[];
    score: number;
    id_: string;
    organism_image: string;
}

export default class ShortyCluster {
    name: string;
    orgid: number;
    documents: ShortyDocument[];
    score: number;
    summary: string[];
    images: ShortyImage[];
    id_: string;
    organism_image: string;

    constructor(name: string, orgid: number, score: number, summary: string[], documents: ShortyDocument[], images: ShortyImage[], id_: string, organism_image: string) {
        this.id_ = id_;
        this.name = name;
        this.orgid = orgid;
        this.score = score;
        this.summary = summary;
        this.documents = documents;
        this.images = images;
        this.organism_image = organism_image;
    }

    static from_json(json: ShortyClusterJson) {
        return new ShortyCluster(
            json.name,
            json.orgid,
            json.score,
            json.summary,
            json.documents.map(x => ShortyDocument.from_json(x)),
            json.images.map(x => ShortyImage.from_json(x)),
            json.id_,
            json.organism_image
        )
    }

    toLightweight(): LightWeightCluster {
        return {
            id_: this.id_,
            name: this.name,
            docids: this.documents.map(x=>x.id),
            orgid: this.orgid,
            score: this.score,
            organism_image: this.organism_image
        }
    }

}