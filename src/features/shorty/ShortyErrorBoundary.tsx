import * as React from "react";
import Logger from "../../models/logger/LoggerService";

interface ErrorProps {
}

interface ErrorBoundaryState {
    hasError: boolean;
}

class ShortyErrorBoundary extends React.Component<ErrorProps, ErrorBoundaryState> {
    constructor(props:ErrorProps) {
        super(props);
        this.state = {hasError: false};
    }

    static getDerivedStateFromError(error:any) {
        // Update state so the next render will show the fallback UI.
        return {hasError: true}
    }

    componentDidCatch(error:any, errorInfo:any) {
        // You can also log the error to an error reporting service
        //logErrorToMyService(error, errorInfo);
        console.log(error);
        console.log(errorInfo);
        Logger.warn("Shorty caught error at boundary" + JSON.stringify(error) + JSON.stringify(errorInfo))
    }

    render() {
        if (this.state.hasError) {
            // You can render any custom fallback UI
            return <div><h1>Sorry. :(</h1><br/>
                <h2>Something went wrong.</h2></div>;
        } else {
            return this.props.children;
        }
    }
}

export default ShortyErrorBoundary;
