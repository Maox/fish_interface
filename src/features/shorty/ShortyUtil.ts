/**
 * Created by Mart Willocx on 21/06/2019 - 10:54.
 *
 */
import {Map} from "immutable"

export function ShortyRenderCommaWords(words: string[]): string {
    /*
     * Render the words in the list with some comma's and spaces!
     */
    let out = "";
    if (words.length < 1) {
        return "none"
    }
    for (let i = 0; i < words.length; i++) {
        const word = words[i];
        out += word;
        if (i < words.length - 1) {
            out += ", "
        }
    }
    return out
}

export function ShortyMapToFilteredList(amap: Map<any, boolean>): any[] {
    /*
        Takes an immutable map with keys and bools and returns the keys wich are True
     */
    return Array.from(amap.keys()).filter(x => amap.get(x, false))
}

export function toSentenceCase(sentence: string): string {
    // https://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript/64910248#64910248
    if(sentence.length < 2){return sentence}
    return sentence.split(' ').map(w => w[0].toUpperCase() + w.substr(1).toLowerCase()).join(' ');
}

export function limitStringLength(sentence: string, max_length: number):string{
    if(sentence.length>max_length){
        sentence = sentence.slice(0,max_length) + "...";
    }
    return sentence
}