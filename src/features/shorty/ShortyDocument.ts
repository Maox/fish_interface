/**
 * Created by Mart Willocx on 28/05/2020 - 16:34.
 *
 */

export interface ShortyDocumentJson {
    id: number
    weight: number
    title: string
    doi:string
    docid:number
    abstract:string
}

export default class ShortyDocument {
    id: number;
    title: string;
    weight: number;
    doi:string;
    docid:number;
    abstract:string;

    constructor(id:number, title:string, weight:number, doi:string, docid:number, abstract:string) {
        this.id = id;
        this.title = title;
        this.weight = weight;
        this.doi = doi;
        this.docid = docid;
        this.abstract = abstract;
    }

    static from_json(json: ShortyDocumentJson) {
        return new ShortyDocument(json.id, json.title, json.weight, json.doi, json.docid, json.abstract)
    }

}