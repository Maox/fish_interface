import * as React from "react";
import {Figure} from "react-bootstrap";
import ShortyCluster from "./ShortyCluster";
import {limitStringLength, toSentenceCase} from "./ShortyUtil";
import {connect} from "react-redux";
import ShortyDocument from "./ShortyDocument";
import {AppDispatch, RootState} from "../../appstate/store";
import {UNAVAILABLE} from "../../assets/locations";

export interface OranismMozaicFigureProps {
    //These are the props we should pass to the current component.
    setVisibleCluster: (id: string) => void;
    cluster: ShortyCluster;
    fitness_visibility: boolean;
}

interface OrganismMozaicFigureState{
    error: boolean
}


class OranismMozaicFigure extends React.Component<OranismMozaicFigureProps, OrganismMozaicFigureState> {
    constructor(props: any) {
        super(props);
        this.containsDocID = this.containsDocID.bind(this);
        this.state = { error: false };
    }

    containsDocID(docid: number, documents: ShortyDocument[]) {
        for (let i = 0; i < documents.length; i++) {
            const document = documents[i];
            if (document.id === docid) {
                return true;
            }
        }
    }

    handleImageError() {
        this.setState({error: true});
    }

    render() {
        let special = this.containsDocID(152915, this.props.cluster.documents);
        let style = {};
        if (special) {
            style = {"color": "red"}
        }
        return <Figure
            key={"moz-f-" + this.props.cluster.images[0].src}
        >
            <Figure.Image src={this.state.error ? UNAVAILABLE : this.props.cluster.images[0].src}
                          alt={this.props.cluster.images[0].alt}
                          style={{maxWidth: "inherit", height: "5rem", "margin": "1px"}}
                          key={"moz-i" + this.props.cluster.images[0].src}
                          onClick={() => this.props.setVisibleCluster(this.props.cluster.id_)}
                          onError={this.handleImageError.bind(this)}
            />
            <Figure.Caption style={style}>
                {toSentenceCase(limitStringLength(this.props.cluster.name, 18))}{this.props.fitness_visibility &&
            <span>({this.props.cluster.score.toPrecision(2)})</span>}
            </Figure.Caption>
        </Figure>

    }
}

const mapStateToProps = (state: RootState) => {
    return {
        fitness_visibility: state.ui_settings.fitness_visibility,
    };
};

function mapDispatchToProps(dispatch: AppDispatch) {
    return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(OranismMozaicFigure);