import * as React from "react";
import {Col, Image,Button} from "react-bootstrap";

export interface ExtractedImageRenderProps {
    //These are the props we should pass to the current component.
    src: string,
    caption: string,
    alt: string
}

interface ExtractedImageRenderState {
    opened: boolean
}

class ExtractedImageRender extends React.Component<ExtractedImageRenderProps, ExtractedImageRenderState> {
    constructor(props:any) {
        super(props);
        this.state = {
            opened: false
        }
        this.toggleOpen = this.toggleOpen.bind(this);
    }

    toggleOpen() {
        this.setState({opened: !this.state.opened});
    }


    render() {

        let caption = this.props.caption;
        if (!this.state.opened) {
            // Truncate the description
            caption = caption.slice(0, 140) + "...";
        }

        return <Col>
            <Image src={this.props.src} alt={this.props.alt} style={{maxWidth: "inherit", maxHeight: "15rem"}}/>
            <p>{caption}
                {!this.state.opened && <Button onClick={() => this.toggleOpen()}> See more.</Button>}
            </p>
        </Col>
    }
}


export default ExtractedImageRender;