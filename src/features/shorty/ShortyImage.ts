/**
 * Created by Mart Willocx on 28/05/2020 - 16:34.
 *
 */

export interface ShortyImageJson {
    weight: number
    caption: string
    src: string
    alt: string
}

export default class ShortyImage {
    weight: number;
    caption: string;
    src: string;
    alt: string;

    constructor(src:string, caption:string, alt:string, weight:number) {
        this.src = src;
        this.caption = caption;
        this.alt = alt;
        this.weight = weight;
    }

    static from_json(json: ShortyImageJson) {
        return new ShortyImage(json.src, json.caption, json.alt, json.weight)
    }

}