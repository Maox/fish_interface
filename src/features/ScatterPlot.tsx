// @ts-ignore
import React from 'react';
// import OrganismMozaic from "./shorty/OranismMozaic";
// import CustomErrorBoundary from "./errors/CustomErrorBoundary";
import {RootState} from "../appstate/store";
import {connect} from "react-redux";

// @ts-ignore
import * as d3 from "d3next";

// @ts-ignore

export interface DocumentLoadingPlotProps {
    xtitle: string
    ytitle: string
    data: number[]
}

class ScatterPlot extends React.Component<DocumentLoadingPlotProps, {}> {

    componentDidMount() {
        console.log(this.props.data)

        let data = this.props.data.map((val:number, index:number) => {return {"x":index, "y": val}});
        let div_id = "#scatterplot";

        var margin = {top: 10, right: 30, bottom: 30, left: 60},
            width = 460 - margin.left - margin.right,
            height = 400 - margin.top - margin.bottom;

        // append the svg object to the body of the page
        var svg = d3.select(div_id)
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform",
                "translate(" + margin.left + "," + margin.top + ")");

        // Add X axis
        let min_x = d3.min(data, function(d:any){return +d.x});
        let max_x = d3.max(data, function(d:any){return +d.x});

        var x = d3.scaleLinear()
            .domain([min_x, max_x])
            .range([0, width]);
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x));

        // Add Y axis
        // let min_y = d3.min(data, function(d:any){return +d.y});
        let max_y = d3.max(data, function(d:any){return +d.y});

        var y = d3.scaleLinear()
            .domain([0, max_y])
            .range([height, 0]);
        svg.append("g")
            .call(d3.axisLeft(y));

        // Add X axis label:
        svg.append("text")
            .attr("text-anchor", "end")
            .attr("x", width)
            .attr("y", height + margin.top + 20)
            .text(this.props.xtitle);

        // Y axis label:
        svg.append("text")
            .attr("text-anchor", "end")
            .attr("transform", "rotate(-90)")
            .attr("y", -margin.left+30)
            .attr("x", -margin.top)
            .text(this.props.ytitle)

            // Add dots
            svg.append('g')
                .selectAll("dot")
                .data(data)
                .enter()
                .append("circle")
                    .attr("cx", function (d: any) {
                        return x(d.x);
                    })
                    .attr("cy", function (d: any) {
                        return y(d.y);
                    })
                    .attr("r", "1")
                    .style("fill", "#69b3a2")


    }


    render() {
        return <div id={"scatterplot"}></div>
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        // document_weights: state.cluster.cluster_data.extra_data.document_weights,
        // document_weightsd2: state.cluster.cluster_data.extra_data.document_weights_2d,
    };
};

export default connect(mapStateToProps, {})(ScatterPlot);