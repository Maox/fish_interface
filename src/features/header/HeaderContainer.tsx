import React, {Dispatch} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {connect} from "react-redux";
import {RootState} from "../../appstate/store";
import {isProduction} from "../../models/utils";
import {toggleSearchSettingsVisibility} from '../../appstate/reducers/searchSettingsReducer';
import {Action} from '@reduxjs/toolkit';

export interface HeaderContainerProps {
    server_status: string,
    search_settings_visible: boolean
    toggleSearchSettingsVisibility: () => void
}

class HeaderContainer extends React.Component<HeaderContainerProps, {}> {
    render() {

        let open_or_close: string = this.props.search_settings_visible ? "Close" : "Open";

        return <div>
            <Row>
                <Col>
                    <a className="navbar-brand" href="/search">Functional search for organism strategies</a>
                </Col>
                <Col>
                    Server Status: {this.props.server_status}
                    {/*{this.props.db && "Looking in database: " + this.props.db.friendly_name}*/}

                    {/*{changebutton}*/}
                    {/*{this.props.show_export && <ExportButton/>}*/}
                    &nbsp;&nbsp;&nbsp;{!isProduction() &&
                <Button
                    onClick={() => {
                        console.log("clicked.");
                        this.props.toggleSearchSettingsVisibility()
                    }}
                >{open_or_close} settings</Button>}
                </Col>
            </Row>
            {/*{ !isProduction() && <Row> <SearchSettings resetSearch={this.props.resetSearch}/>*/}
            {/*</Row>}*/}

        </div>;
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        server_status: state.server_status.status,
        search_settings_visible: state.search_settings.settings_visible
    };
};

function mapDispatchToProps(dispatch: Dispatch<Action>) {
    return {
        toggleSearchSettingsVisibility: () => dispatch(toggleSearchSettingsVisibility())
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer)
