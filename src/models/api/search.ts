import {get_search_host, postData} from "../utils";

export interface SearchRequestInterface {
    query: SearchQueryInterface
    search_settings: SearchSettingsInterface
}

export interface SearchSettingsInterface {
    db: string;
    // search_method: string
    filter_lemmatized_query: boolean;
    // idcookie: string
}
export interface SearchQueryInterface {
    tokenized_query: string[];
    to_be_tokenized: string
}

export interface QueryResult{
    query_info: QueryResultInfo
    result: Array<number>[]
    kneepoint: number
    is_error: boolean
}

export interface QueryResultInfo{
    tech_words: Array<[number,string]>[]
    bio_words: Array<[number,string]>[]
    functional_words: Array<[number,string]>[]
    cleaned_query: string[]
    warning: string
    weighted_lemmas: Array<[number,string]>[]
    time_taken: number
    from_cache: boolean
    lemma_suggestions: string[]
}

export function get_search_results(query: SearchRequestInterface): Promise<QueryResult> {
    return postData(get_search_host() + "search", query)
}