import {get_search_host, postData} from "../utils";
import {Cluster} from "cluster";
import ShortyCluster, {LightWeightCluster} from "../../features/shorty/ShortyCluster";

export interface ClusterRequestInterface {
    db: string
    cluster_settings: ClusterSettings
}

export interface ClusterSettings {
    selected_oas: number[]
    oa_weights: number[]
    // amount_documents: number[]
    db_name: string
    scoring_method: string
    clustering_method:string
}

export interface ClusterResult {
    cluster_info: any
    clustering_method: string
    data: any
}

export interface SummarizationRequest{
    summarization_settings: SummarySettings,
    cluster: LightWeightCluster
}

export interface SummarySettings{

}

export interface SummarizationResult{
    result: ShortyCluster
}

export function get_clustering_results(cluster_request: ClusterRequestInterface): Promise<object> {
    return postData(get_search_host() + "cluster", cluster_request)
}

export function get_summary(summarization_request: SummarizationRequest): Promise<SummarizationResult>{
    return postData(get_search_host() + "summarize", summarization_request)
}