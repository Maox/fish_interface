import {createLogger, format, transports} from "winston";

function getCookie(name:string) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts && parts.length === 2) {
      // @ts-ignore
      return parts.pop().split(';').shift();
  }
}

const add_cookie = format((info, opts) => {
    // console.log("Adding cookie", getCookie("id"));
    info.message = getCookie("id") + "-" +  info.message;
  return info;
});

// @ts-ignore
let tr = new transports.Http({
    host: "log.aves.willocx.be",
    path: "/",
    level: 'warn',
    ssl: true,
    format: format.combine(
        add_cookie(),
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        // format.colorize(),
        format.splat(),
        format.json(),
        format.simple()
    ),
});
const Logger = createLogger({
    level: 'info',
    defaultMeta: {service: 'FISH-main'},
    transports: [
        //
        // - Write to all logs with level `info` and below to `quick-start-combined.log`.
        // - Write all logs error (and below) to `quick-start-error.log`.
        //
        new transports.Console({
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                // format.colorize(),
                format.splat(),
                format.json(),
                format.simple()
            ),
        }),
        // @ts-ignore
        tr
    ]
});


export default Logger;