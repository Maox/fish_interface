/**
 * Created by Mart Willocx on 21/06/2019 - 10:54.
 *
 */

export function postData(url = '', data = {}) {
    // Default options are marked with *
    return fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
        .then(response => {
            if (!response.ok) {
                throw Error(response.statusText);
            }
            return response.json()
        }); // parses JSON response into native Javascript objects
}

export function renderCommaWords(words: string[]) {
    /*
     * Render the words in the list with some comma's and spaces!
     */
    let out = "";
    if (words.length < 1) {
        return "none"
    }
    for (let i = 0; i < words.length; i++) {
        const word = words[i];
        out += word;
        if (i < words.length - 1) {
            out += ", "
        }
    }
    return out
}

export function cleanWord(word: string) {
    if (word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    } else {
        return word;
    }
}

export class ToggleList {
    list: string[]

    constructor() {
        this.list = []
    }

    toggle(id: string) {
        if (this.list.indexOf(id) > -1) {
            this.list.splice(this.list.indexOf(id))
        } else {
            this.list.push(id)
        }
    }

    is_checked(id: string) {
        return this.list.indexOf(id) > -1;
    }

    get_list() {
        return this.list;
    }

    set_list(list: string[]) {
        this.list = list;
    }
}

export function toggleItemInList(list: string[], id: string) {
    /*
     * Toggle the presence of the id in the list.
     * This is an immutable function ;-)
     */
    let new_list = list.slice();
    if (list.indexOf(id) > -1) {
        list.splice(list.indexOf(id))
    } else {
        list.push(id)
    }
    return list
}

export function pushItemInList(array: any[], item: any) {
    return insertItemInList(array, array.length, item)
}

export function insertItemInList(array: any[], index: number, item: any) {
    let newArray = array.slice();
    newArray.splice(index, 0, item);
    return newArray
}

export function removeItemFromList(array: any[], index: number) {
    let newArray = array.slice();
    newArray.splice(index, 1);
    return newArray
}

export function mapToFilteredList(amap: any) {
    return Array.from(amap.keys()).filter(x => amap.get(x, false))
}

export function reworkInArray(list: any[], index: number, funct: any) {
    /*
     * Rework the value in the array using the function that returns an array of values.
     *
     * reworkInArray([1,2,3], 1, (a)=>a**2) => [1,4,3]
     * reworkInArray([1,2,3], 1, (a)=>{let out = []; while(a>0){out.push(a); a--;} return out;} => [1,2,1,0,3]
     */
    return list.slice(0, index).concat(funct(list[index]).concat(list.slice(index + 1)));
}

export function isProduction(): boolean {
    return !!window.location.hostname.match('willocx');
}

export function set_willocx_be_cookie() {
    let cookieName = 'test';
    var cookieValue = 'testjqsmlqlksm2324IZE';
    var myDate = new Date();
    myDate.setMonth(myDate.getMonth() + 12);
    document.cookie = cookieName + "=" + cookieValue + ";expires=" + myDate
        + ";domain=willocx.be;path=/";
}

export function set_tracking_cookie() {
    console.log("Setting cookie");
    const urlParams = new URLSearchParams(window.location.search);
    const userID = urlParams.get('id');
    var myDate = new Date();
    myDate.setMonth(myDate.getMonth() + 12);
    document.cookie = "id=" + userID + ";expires=" + myDate
        + ";domain=willocx.be;path=/";
}

export function get_search_host(): string {
    let search_host = "http://localhost:8080/";
    if (window.location.href.includes("aves.willocx.be")) {
        search_host = "https://mapping.aves.willocx.be/";
    }
    if (window.location.href.includes("aves3.willocx.be")) {
        search_host = "https://mapping.aves3.willocx.be/";
    }
    if (window.location.href.includes("bela.willocx.be")) {
        search_host = "http://mapping.bela.willocx.be/";
    }
    return search_host
}

export function get_data_domain(): string {
    let data_domain = "http://landbird.localhost/";
    if (window.location.href.includes("aves.willocx.be")) {
        data_domain = "https://aves.willocx.be/";
    }
    if (window.location.href.includes("aves3.willocx.be")) {
        data_domain = "https://aves3.willocx.be/";
    }
    return data_domain
}